using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

/// <summary>
/// An abstract class for the GUI that allows the user to change the current set of a car element and to edit its material properties.
/// 
/// For example, to change the current wheels set and to edit its material.
/// </summary>
/// <typeparam name="T1"></typeparam>
/// <typeparam name="T2"></typeparam>
public abstract class AbstractSetsWithMaterialsChooserGUI<T1, T2> : MaterialChangerGUI where T1 : AbstractSetsWithMaterialsChooser<T2> where T2 : AbstractConfigurator, IMaterialChanger
{
    public Button previousSetButton = null;
    public Button nextSetButton = null;

    public TMPro.TextMeshProUGUI currentSetNumberTextField = null;

    protected override void Awake()
    {
        base.Awake();

        if (previousSetButton == null)
        {
            var _ = Utils.FindTransformsWithName(transform, "Previous Set Button", false);
            if (_.Count > 0)
                previousSetButton = _[0].GetComponent<Button>();
        }

        if (nextSetButton == null)
        {
            var _ = Utils.FindTransformsWithName(transform, "Next Set Button", false);
            if (_.Count > 0)
                nextSetButton = _[0].GetComponent<Button>();
        }

        if (currentSetNumberTextField == null)
        {
            var _ = Utils.FindTransformsWithName(transform, "Current Set Number Text Field", false);
            if (_.Count > 0)
                currentSetNumberTextField = _[0].GetComponent<TMPro.TextMeshProUGUI>();
        }

        Assert.IsNotNull(previousSetButton);
        Assert.IsNotNull(nextSetButton);
        Assert.IsNotNull(currentSetNumberTextField);
    }

    public override void OnCurrentCarChanged()
    {
        base.OnCurrentCarChanged();

        ChangeSet(0);
    }

    public virtual void OnClickedPreviousSetButton()
    {
        ChangeSet(-1);
    }

    public virtual void OnClickedNextSetButton()
    {
        ChangeSet(1);
    }

    protected virtual void ChangeSet(int direction)
    {
        if (SetsWithMaterialsChooser != null)
        {
            SetsWithMaterialsChooser.CurrentSetNumber
            = Mathf.Clamp(
                SetsWithMaterialsChooser.CurrentSetNumber + direction,
                allowCurrentSetNegativeNumber ? -1 : 0,
                SetsWithMaterialsChooser.setsConfigurators.Count - 1);
            // Set to 0.
            ChangeCurrentElement(-currentElementNumber);
        }

        carConfiguratorGUI.HandleColorPickerAndMaterialPropertiesSetter();
        EnableDisableSetChoiceButtons();
        SetCurrentSetNumberText();
    }

    protected virtual void SetCurrentSetNumberText()
    {
        if (SetsWithMaterialsChooser == null || (!allowCurrentSetNegativeNumber && TotalSetsNumber < 2) || (allowCurrentSetNegativeNumber && SetsWithMaterialsChooser.CurrentSetNumber < 0))
            currentSetNumberTextField.text = "x";
        else
            currentSetNumberTextField.text = (SetsWithMaterialsChooser.CurrentSetNumber + 1).ToString();
    }

    protected virtual void EnableDisableSetChoiceButtons()
    {
        if ((allowCurrentSetNegativeNumber && TotalSetsNumber < 1) || (!allowCurrentSetNegativeNumber && TotalSetsNumber < 2))
        {
            previousSetButton.interactable = false;
            nextSetButton.interactable = false;
        }
        else
        {
            previousSetButton.interactable = true;
            nextSetButton.interactable = true;
        }
    }

    protected virtual int TotalSetsNumber
    {
        get
        {
            return SetsWithMaterialsChooser != null ? SetsWithMaterialsChooser.setsConfigurators.Count : 0;
        }
    }

    protected override List<Color> Colors
    {
        get
        {
            return SetsWithMaterialsChooser != null ? SetsWithMaterialsChooser.Colors : new List<Color>(); ;
        }

        set
        {
            if (SetsWithMaterialsChooser != null)
                SetsWithMaterialsChooser.Colors = value;
        }
    }

    protected override List<float> MetallicAttributes
    {
        get
        {
            return SetsWithMaterialsChooser != null ? SetsWithMaterialsChooser.MetallicAttributes : new List<float>();
        }

        set
        {
            if (SetsWithMaterialsChooser != null)
                SetsWithMaterialsChooser.MetallicAttributes = value;
        }
    }

    protected override List<float> SmoothnessAttributes
    {
        get
        {
            return SetsWithMaterialsChooser != null ? SetsWithMaterialsChooser.SmoothnessAttributes : new List<float>();
        }

        set
        {
            if (SetsWithMaterialsChooser != null)
                SetsWithMaterialsChooser.SmoothnessAttributes = value;
        }
    }

    public abstract T1 SetsWithMaterialsChooser
    {
        get;
    }
}
