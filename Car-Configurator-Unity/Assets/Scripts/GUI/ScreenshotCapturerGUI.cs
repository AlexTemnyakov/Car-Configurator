using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotCapturerGUI : MonoBehaviour
{
    private bool capturingScreenshot = false;

    public void OnCpatureScreenshotButtonClicked()
    {
        StartCoroutine(CaptureScreenshot());
    }

    public IEnumerator CaptureScreenshot()
    {
        if (capturingScreenshot)
            yield break;

        capturingScreenshot = true;

        yield return null;

        HashSet<Canvas> allCanvases = new HashSet<Canvas>(Utils.FindObjectsWithComponent<Canvas>());

        Dictionary<Canvas, bool> enabledAndDisabledCanvases = new Dictionary<Canvas, bool>();

        foreach (var c in allCanvases)
        {
            enabledAndDisabledCanvases[c] = c.enabled;
            c.enabled = false;
        }

        yield return new WaitForEndOfFrame();

        ScreenCapture.CaptureScreenshot("Screenshot_" + Time.realtimeSinceStartup + ".png");

        foreach (var c in allCanvases)
            c.enabled = enabledAndDisabledCanvases[c];

        capturingScreenshot = false;
    }
}
