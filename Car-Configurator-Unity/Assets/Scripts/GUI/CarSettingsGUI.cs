using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

/// <summary>
/// A class for the GUI that allows the user to change the current car and to switch its lights.
/// </summary>
public class CarSettingsGUI : HideableGUI, ICarGUI
{
    public Button switchFrontLightsButton = null;
    public Button switchInteriorLightsButton = null;
    public Button nextCarButton = null;
    public Button previousCarButton = null;

    public CarChooser carChooser = null;
    public CarConfiguratorGUI carConfiguratorGUI = null;

    protected override void Start()
    {
        base.Start();

        carChooser = Utils.FindObjectsWithComponent<CarChooser>()[0];
        carConfiguratorGUI = Utils.FindObjectsWithComponent<CarConfiguratorGUI>()[0];

        Assert.IsNotNull(switchFrontLightsButton);
        Assert.IsNotNull(switchInteriorLightsButton);
        Assert.IsNotNull(nextCarButton);
        Assert.IsNotNull(previousCarButton);
        Assert.IsNotNull(carConfiguratorGUI);

        OnCurrentCarChanged();
    }

    public void OnSwitchCarFrontLightsButtonClicked()
    {
        carChooser.CurrentCarConfigurator.frontLightsConfigurator.SwitchLights();
    }

    public void OnSwitchCarInteriorLightsButtonClicked()
    {
        carChooser.CurrentCarConfigurator.interiorLightsConfigurator.SwitchLights();
    }

    public void OnNextCarButtonClicked()
    {
        carChooser.NextCar();
        OnCurrentCarChanged();
    }

    public void OnPreviousCarButtonClicked()
    {
        carChooser.PreviousCar();
        OnCurrentCarChanged();
    }

    public void OnCurrentCarChanged()
    {
        switchFrontLightsButton.interactable = carChooser.CurrentCarConfigurator.frontLightsConfigurator != null;
        switchInteriorLightsButton.interactable = carChooser.CurrentCarConfigurator.interiorLightsConfigurator != null;

        carConfiguratorGUI.OnCurrentCarChanged();
    }
}
