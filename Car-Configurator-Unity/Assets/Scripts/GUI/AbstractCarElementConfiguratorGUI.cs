using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractCarElementConfiguratorGUI : MonoBehaviour, ICarGUI
{
    public CarConfiguratorGUI carConfiguratorGUI = null;

    protected virtual void Awake()
    {
        if (carConfiguratorGUI == null)
            carConfiguratorGUI = Utils.FindObjectOfType<CarConfiguratorGUI>();
    }

    protected virtual void Start()
    {

    }

    public virtual void OnCurrentCarChanged()
    {

    }
}
