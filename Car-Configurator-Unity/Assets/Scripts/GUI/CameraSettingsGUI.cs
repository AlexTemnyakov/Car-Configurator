using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class for the GUI for the camera.
/// It has buttons to move the camera to a car element.
/// </summary>
public class CameraSettingsGUI : HideableGUI
{
    public CameraWithTrackball carConfiguratorCamera = null;

    protected override void Start()
    {
        base.Start();

        carConfiguratorCamera = Utils.FindObjectsWithComponent<CameraWithTrackball>()[0];
    }

    void Update()
    {
        
    }

    public void OnBodyButtonClicked()
    {
        carConfiguratorCamera.ToCarBody();
    }

    public void OnFLWheelButtonClicked()
    {
        carConfiguratorCamera.ToCarFLWheel();
    }

    public void OnFRWheelButtonClicked()
    {
        carConfiguratorCamera.ToCarFRWheel();
    }

    public void OnRLWheelButtonClicked()
    {
        carConfiguratorCamera.ToCarRLWheel();
    }

    public void OnRRWheelButtonClicked()
    {
        carConfiguratorCamera.ToCarRRWheel();
    }
}
