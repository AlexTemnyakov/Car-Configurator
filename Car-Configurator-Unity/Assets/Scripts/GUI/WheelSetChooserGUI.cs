using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class WheelSetChooserGUI : AbstractSetsWithMaterialsChooserGUI<WheelSetChooser, WheelSetConfigurator>
{   
    protected override void Start()
    {
        base.Start();

        allowCurrentSetNegativeNumber = false;
    }

    public override WheelSetChooser SetsWithMaterialsChooser
    {
        get
        {
            return carConfiguratorGUI.CurrentCarConfigurator.wheelSetChooser;
        }
    }
}
