using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public abstract class AbstractConfiguratorWithMaterialsGUI<T> : MaterialChangerGUI where T : IMaterialChanger
{
    protected override void Awake()
    {
        base.Awake();

        Assert.IsNotNull(currentElementNumberTextField);
    }

    protected override List<Color> Colors
    {
        get
        {
            return AbstractConfiguratorWithMaterials != null ? AbstractConfiguratorWithMaterials.Colors : new List<Color>();
        }

        set
        {
            if (AbstractConfiguratorWithMaterials != null)
                AbstractConfiguratorWithMaterials.Colors = value;
        }
    }

    protected override List<float> MetallicAttributes
    {
        get
        {
            return AbstractConfiguratorWithMaterials != null ? AbstractConfiguratorWithMaterials.MetallicAttributes : new List<float>();
        }

        set
        {
            if (AbstractConfiguratorWithMaterials != null)
                AbstractConfiguratorWithMaterials.MetallicAttributes = value;
        }
    }

    protected override List<float> SmoothnessAttributes
    {
        get
        {
            return AbstractConfiguratorWithMaterials != null ? AbstractConfiguratorWithMaterials.SmoothnessAttributes : new List<float>();
        }

        set
        {
            if (AbstractConfiguratorWithMaterials != null)
                AbstractConfiguratorWithMaterials.SmoothnessAttributes = value;
        }
    }

    public abstract T AbstractConfiguratorWithMaterials
    {
        get;
    }
}
