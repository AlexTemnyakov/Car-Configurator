using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An interface that requires to implement a function which is supposed to be called when the current car in the scene is changed.
/// </summary>
public interface ICarGUI
{
    public void OnCurrentCarChanged();
}
