using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

/// <summary>
/// An abstract class for the GUI which allows the user to change a material's parameters.
/// </summary>
public abstract class MaterialChangerGUI : AbstractCarElementConfiguratorGUI
{
    public Button previousElementButton = null;
    public Button nextElementButton = null;

    public TMPro.TextMeshProUGUI currentElementNumberTextField = null;

    [SerializeField]
    protected int currentElementNumber = 0;

    protected bool allowCurrentSetNegativeNumber = true;

    protected override void Awake()
    {
        base.Awake();

        if (previousElementButton == null)
        {
            var _ = Utils.FindTransformsWithName(transform, "Previous Element Button", false);
            if (_.Count > 0)
                previousElementButton = _[0].GetComponent<Button>();
        }

        if (nextElementButton == null)
        {
            var _ = Utils.FindTransformsWithName(transform, "Next Element Button", false);
            if (_.Count > 0)
                nextElementButton = _[0].GetComponent<Button>();
        }

        if (currentElementNumberTextField == null)
        {
            var _ = Utils.FindTransformsWithName(transform, "Current Element Number Text Field", false);
            if (_.Count > 0)
                currentElementNumberTextField = _[0].GetComponent<TMPro.TextMeshProUGUI>();
        }

        Assert.IsNotNull(currentElementNumberTextField);
        Assert.IsNotNull(previousElementButton);
        Assert.IsNotNull(nextElementButton);
    }    

    protected override void Start()
    {
        base.Start();

        OnCurrentCarChanged();
    }

    public override void OnCurrentCarChanged()
    {
        ChangeCurrentElement(0);
    }

    public virtual void OnClickedNextElement()
    {
        ChangeCurrentElement(1);
    }

    public virtual void OnClickedPreviousElement()
    {
        ChangeCurrentElement(-1);
    }

    protected virtual void ChangeCurrentElement(int direction)
    {
        currentElementNumber = Mathf.Clamp(currentElementNumber + direction, 0, TotalElementsNumber - 1);

        SetCurrentElementNumberText();
        EnableDisableElementChoiceButtons();

        carConfiguratorGUI.HandleColorPickerAndMaterialPropertiesSetter();
    }

    protected virtual void SetCurrentElementNumberText()
    {
        if (TotalElementsNumber < 2)
            currentElementNumberTextField.text = "x";
        else
            currentElementNumberTextField.text = (currentElementNumber + 1).ToString();
    }

    protected virtual void EnableDisableElementChoiceButtons()
    {
        if (TotalElementsNumber < 2)
        {
            previousElementButton.interactable = false;
            nextElementButton.interactable = false;
        }
        else
        {
            previousElementButton.interactable = true;
            nextElementButton.interactable = true;
        }
    }

    public virtual int TotalElementsNumber
    {
        get
        {
            return Mathf.Max(Colors.Count, MetallicAttributes.Count, SmoothnessAttributes.Count);
        }
    }

    public virtual Color? CurrentElementColor
    {
        get
        {
            if (Colors.Count > 0 && currentElementNumber >= 0 && Colors.Count > 0)
                return Colors[currentElementNumber];
            else
                return null;
        }

        set
        {
            if (value.HasValue)
            {
                var _ = Colors;

                if (currentElementNumber < Colors.Count)
                {
                    _[currentElementNumber] = value.Value;
                    Colors = _;
                }
            }
        }
    }

    public virtual float? CurrentElementMetallic
    {
        get
        {
            if (MetallicAttributes.Count > 0 && currentElementNumber >= 0 && MetallicAttributes.Count > 0)
                return MetallicAttributes[currentElementNumber];
            else
                return null;
        }

        set
        {
            if (value.HasValue)
            {
                var _ = MetallicAttributes;

                if (currentElementNumber < MetallicAttributes.Count)
                {
                    _[currentElementNumber] = value.Value;
                    MetallicAttributes = _;
                }
            }
        }
    }

    public virtual float? CurrentElementSmoothness
    {
        get
        {
            if (SmoothnessAttributes.Count > 0 && currentElementNumber >= 0 && SmoothnessAttributes.Count > 0)
                return SmoothnessAttributes[currentElementNumber];
            else
                return null;
        }

        set
        {
            if (value.HasValue)
            {
                var _ = SmoothnessAttributes;

                if (currentElementNumber < SmoothnessAttributes.Count)
                {
                    _[currentElementNumber] = value.Value;
                    SmoothnessAttributes = _;
                }
            }
        }
    }

    protected abstract List<Color> Colors
    {
        get;
        set;
    }

    protected abstract List<float> MetallicAttributes
    {
        get;
        set;
    }

    protected abstract List<float> SmoothnessAttributes
    {
        get;
        set;
    }
}
