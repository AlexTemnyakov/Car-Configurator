using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButtonGUI : MonoBehaviour
{
    public void OnQuitButtonClicked()
    {
        Application.Quit();
    }
}
