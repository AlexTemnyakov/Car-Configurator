using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyConfiguratorGUI : AbstractConfiguratorWithMaterialsGUI<BodyConfigurator>
{    
    protected override void Awake()
    {
        base.Awake();

        allowCurrentSetNegativeNumber = false;        
    }

    public override BodyConfigurator AbstractConfiguratorWithMaterials
    {
        get
        {
            return carConfiguratorGUI.CurrentCarConfigurator.bodyConfigurator;
        }
    }
}
