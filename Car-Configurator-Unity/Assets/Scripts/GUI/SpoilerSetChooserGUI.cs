using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpoilerSetChooserGUI : AbstractSetsWithMaterialsChooserGUI<SetsWithMaterialsChooser, ConfiguratorWithMaterials>
{
    public override SetsWithMaterialsChooser SetsWithMaterialsChooser
    {
        get
        {
            return carConfiguratorGUI.CurrentCarConfigurator.spoilerChooser;
        }
    }
}
