using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An abstract class for the GUI that the user may hide.
/// </summary>
public abstract class HideableGUI : MonoBehaviour
{
    public Canvas guiCanvas = null;

    protected bool hideControlElements = true;

    virtual protected void Start()
    {
        OnShowHideButtonClicked();
    }

    public virtual void OnShowHideButtonClicked()
    {
        guiCanvas.enabled = !hideControlElements;

        hideControlElements = !hideControlElements;
    }
}
