using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

/// <summary>
/// A class for the GUI to change the scene parameters.
/// 
/// That is, to switch day/night and to adjust the lighting in the garage.
/// </summary>
public class SceneSettingsGUI : HideableGUI
{
    public LightingManager lightingManager = null;
    public Transform indoorLightingRoot = null;
    public Transform dayNightSwitchButtonTransform = null;
    public Transform indoorLightingIntensityPanelRoot = null;

    private Slider indoorLightingIntensitySlider = null;
    private List<Light> indoorLightSources = new List<Light>();

    private List<Canvas> guiCanvases = null;

    protected override void Start()
    {
        base.Start();

        Assert.IsNotNull(lightingManager);
        Assert.IsNotNull(indoorLightingRoot);
        Assert.IsNotNull(dayNightSwitchButtonTransform);
        Assert.IsNotNull(indoorLightingIntensityPanelRoot);

        indoorLightingIntensitySlider = indoorLightingIntensityPanelRoot.GetComponentInChildren<Slider>();

        Assert.IsNotNull(indoorLightingIntensitySlider);

        indoorLightingIntensitySlider.onValueChanged.AddListener(delegate { OnIndoorLightingIntensitySliderValueChanged(); });

        indoorLightSources.AddRange(indoorLightingRoot.GetComponentsInChildren<Light>());

        if (indoorLightSources.Count > 0)
            indoorLightingIntensitySlider.value = indoorLightSources[0].intensity;
        indoorLightingIntensitySlider.value = 0;
    }

    private void Update()
    {
        
    }

    public void OnSwitchDayNightButtonClicked()
    {
        lightingManager.SwitchDayNight();
    }

    public void OnIndoorLightingIntensitySliderValueChanged()
    {
        foreach (var l in indoorLightSources)
            l.intensity = indoorLightingIntensitySlider.value;
    }
}
