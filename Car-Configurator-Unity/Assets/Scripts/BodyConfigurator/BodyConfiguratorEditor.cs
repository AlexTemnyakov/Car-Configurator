#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BodyConfigurator))]
public class BodyConfiguratorEditor : AbstractConfiguratorWithMaterialEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BodyConfigurator __target = target as BodyConfigurator;
    }
}

#endif