using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// A manager for the implemented car elements.
/// </summary>
public class CarConfigurator : AbstractConfigurator
{
    // The script finds these configurators automatically. They must be under the transformation of this object.
    public BodyConfigurator bodyConfigurator = null;
    public SpoilerChooser spoilerChooser = null;
    public ScoopChooser scoopChooser = null;
    public WheelSetChooser wheelSetChooser = null;
    public CarFrontLightsConfigurator frontLightsConfigurator = null;
    public CarInteriorLightsConfigurator interiorLightsConfigurator = null;

    public override void Initialize()
    {
        FindWheelSetCooser();
        FindBodyConfigurator();
        FindSpoilerChooser();
        FindScoopChooser();
        FindLightsConfigurators();
    }

    private void FindLightsConfigurators()
    {
        CarFrontLightsConfigurator[] fLights = transform.GetComponentsInChildren<CarFrontLightsConfigurator>();
        if (fLights != null && fLights.Length > 0)
        {
            Assert.AreEqual(fLights.Length, 1);
            frontLightsConfigurator = fLights[0];
        }

        CarInteriorLightsConfigurator[] iLights = transform.GetComponentsInChildren<CarInteriorLightsConfigurator>();
        if (iLights != null && iLights.Length > 0)
        {
            Assert.AreEqual(iLights.Length, 1);
            interiorLightsConfigurator = iLights[0];
        }
    }
    
    private void FindBodyConfigurator()
    {
        BodyConfigurator[] _ = transform.GetComponentsInChildren<BodyConfigurator>();
        Assert.IsNotNull(_);
        Assert.AreEqual(_.Length, 1);
        bodyConfigurator = _[0].GetComponent<BodyConfigurator>();
    }   

    private void FindSpoilerChooser()
    {
        SpoilerChooser[] _ = transform.GetComponentsInChildren<SpoilerChooser>();
        if (_ != null && _.Length > 0)
            spoilerChooser = _[0].GetComponent<SpoilerChooser>();
    }

    private void FindScoopChooser()
    {
        ScoopChooser[] _ = transform.GetComponentsInChildren<ScoopChooser>();
        if (_ != null && _.Length > 0)
            scoopChooser = _[0].GetComponent<ScoopChooser>();
    }
    
    private void FindWheelSetCooser()
    {
        WheelSetChooser[] _ = transform.GetComponentsInChildren<WheelSetChooser>();
        if (_ != null && _.Length > 0)
            wheelSetChooser = _[0].GetComponent<WheelSetChooser>();
    } 
}
