#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CarConfigurator))]
public class CarConfiguratorEditor : AbstractConfiguratorEditor
{
    public static readonly int minorEditorGUISpace = 10;
    public static readonly int mediumEditorGUISpace = 15;
    public static readonly int majorEditorGUISpace = 30;

    private bool initializeFoldout = false;
    private bool bodyConfigurationFoldout = false;
    private bool spoilerConfigurationFoldout = false;
    private bool wheelSetChooserFoldout = false;
    private bool lightsConfiguratorFoldout = false;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        CarConfigurator __target = target as CarConfigurator;

        GUILayout.Space(majorEditorGUISpace);

        EditorGUILayout.LabelField("", EditorStyles.helpBox);
        GUILayout.Space(mediumEditorGUISpace);

        initializeFoldout = EditorGUILayout.Foldout(initializeFoldout, "Initialize");
        if (initializeFoldout)
        {
            if (GUILayout.Button("Initialize"))
                __target.Initialize();
        }

        GUILayout.Space(mediumEditorGUISpace);
        EditorGUILayout.LabelField("", EditorStyles.helpBox);
        GUILayout.Space(mediumEditorGUISpace);

        bodyConfigurationFoldout = EditorGUILayout.Foldout(bodyConfigurationFoldout, "Body configuration");
        if (bodyConfigurationFoldout)
        {
            List<Color> previousColors = __target.bodyConfigurator.Colors;
            List<Color> chosenColors = ShowGUIForColors(previousColors);

            if (!Enumerable.SequenceEqual(previousColors, chosenColors))
                __target.bodyConfigurator.Colors = chosenColors;

            EditorGUILayout.LabelField("Metallic", EditorStyles.helpBox);
            List<float> previousMetallicAttributes = __target.bodyConfigurator.MetallicAttributes;
            List<float> chosenMetallicAttributes = ShowGUIForFloats(previousMetallicAttributes);

            if (!Enumerable.SequenceEqual(previousMetallicAttributes, chosenMetallicAttributes))
                __target.bodyConfigurator.MetallicAttributes = chosenMetallicAttributes;

            EditorGUILayout.LabelField("Smoothness", EditorStyles.helpBox);
            List<float> previousSmothnessAttributes = __target.bodyConfigurator.SmoothnessAttributes;
            List<float> chosenSmothnessAttributes = ShowGUIForFloats(previousSmothnessAttributes);

            if (!Enumerable.SequenceEqual(previousSmothnessAttributes, chosenSmothnessAttributes))
                __target.bodyConfigurator.SmoothnessAttributes = chosenSmothnessAttributes;
        }

        GUILayout.Space(mediumEditorGUISpace);
        EditorGUILayout.LabelField("", EditorStyles.helpBox);
        GUILayout.Space(mediumEditorGUISpace);

        if (__target.spoilerChooser != null)
        {
            spoilerConfigurationFoldout = EditorGUILayout.Foldout(spoilerConfigurationFoldout, "Spoiler configuration");
            if (spoilerConfigurationFoldout)
            {
                int prevValue = __target.spoilerChooser.CurrentSetNumber;
                __target.spoilerChooser.CurrentSetNumber = EditorGUILayout.IntSlider(prevValue, -1, __target.spoilerChooser.setsConfigurators.Count - 1);

                if (__target.spoilerChooser.CurrentSetConfigurator != null)
                {
                    __target.spoilerChooser.CurrentSetConfigurator.unchangeableColor
                        = EditorGUILayout.Toggle("Spoiler color unchangeable", __target.spoilerChooser.CurrentSetConfigurator.unchangeableColor);

                    List<Color> previousColors = __target.spoilerChooser.CurrentSetConfigurator.Colors;
                    List<Color> chosenColors = new List<Color>();
                    for (int i = 0; i < previousColors.Count; i++)
                        chosenColors.Add(EditorGUILayout.ColorField("Chosen color", previousColors[i]));

                    if (!Enumerable.SequenceEqual(previousColors, chosenColors))
                        __target.spoilerChooser.CurrentSetConfigurator.Colors = chosenColors;

                    EditorGUILayout.LabelField("Metallic", EditorStyles.helpBox);
                    List<float> previousMetallicAttributes = __target.spoilerChooser.CurrentSetConfigurator.MetallicAttributes;
                    List<float> chosenMetallicAttributes = ShowGUIForFloats(previousMetallicAttributes);

                    if (!Enumerable.SequenceEqual(previousMetallicAttributes, chosenMetallicAttributes))
                        __target.spoilerChooser.CurrentSetConfigurator.MetallicAttributes = chosenMetallicAttributes;

                    EditorGUILayout.LabelField("Smoothness", EditorStyles.helpBox);
                    List<float> previousSmothnessAttributes = __target.spoilerChooser.CurrentSetConfigurator.SmoothnessAttributes;
                    List<float> chosenSmothnessAttributes = ShowGUIForFloats(previousSmothnessAttributes);

                    if (!Enumerable.SequenceEqual(previousSmothnessAttributes, chosenSmothnessAttributes))
                        __target.spoilerChooser.CurrentSetConfigurator.SmoothnessAttributes = chosenSmothnessAttributes;
                }
            }
        }
        else
        {
            EditorGUILayout.LabelField("No spoiler chooser.", EditorStyles.helpBox);
        }

        GUILayout.Space(mediumEditorGUISpace);
        EditorGUILayout.LabelField("", EditorStyles.helpBox);
        GUILayout.Space(mediumEditorGUISpace);

        if (__target.wheelSetChooser != null)
        {
            wheelSetChooserFoldout = EditorGUILayout.Foldout(wheelSetChooserFoldout, "Wheel configuration");
            if (wheelSetChooserFoldout)
            {
                int prevValue = __target.wheelSetChooser.CurrentSetNumber;
                __target.wheelSetChooser.CurrentSetNumber
                    = EditorGUILayout.IntSlider("Current wheels set", __target.wheelSetChooser.CurrentSetNumber, 0, __target.wheelSetChooser.setsConfigurators.Count - 1);

                GUILayout.Space(minorEditorGUISpace);

                __target.wheelSetChooser.CurrentSetConfigurator.UnchangeableRimColor
                        = EditorGUILayout.Toggle(
                            "Unchangeable rim color",
                            __target.wheelSetChooser.CurrentSetConfigurator.UnchangeableRimColor);

                __target.wheelSetChooser.CurrentSetConfigurator.UnchangeableRimColor
                        = EditorGUILayout.Toggle(
                            "Unchangeable rim material",
                            __target.wheelSetChooser.CurrentSetConfigurator.UnchangeableRimMaterial);

                List<Color> previousColors = __target.wheelSetChooser.CurrentSetConfigurator.Colors;
                List<Color> chosenColors = ShowGUIForColors(previousColors);

                if (!Enumerable.SequenceEqual(previousColors, chosenColors))
                    __target.wheelSetChooser.CurrentSetConfigurator.Colors = chosenColors;

                EditorGUILayout.LabelField("Metallic", EditorStyles.helpBox);
                List<float> previousMetallicAttributes = __target.wheelSetChooser.CurrentSetConfigurator.MetallicAttributes;
                List<float> chosenMetallicAttributes = ShowGUIForFloats(previousMetallicAttributes);

                if (!Enumerable.SequenceEqual(previousMetallicAttributes, chosenMetallicAttributes))
                    __target.wheelSetChooser.CurrentSetConfigurator.MetallicAttributes = chosenMetallicAttributes;

                EditorGUILayout.LabelField("Smoothness", EditorStyles.helpBox);
                List<float> previousSmothnessAttributes = __target.wheelSetChooser.CurrentSetConfigurator.SmoothnessAttributes;
                List<float> chosenSmothnessAttributes = ShowGUIForFloats(previousSmothnessAttributes);

                if (!Enumerable.SequenceEqual(previousSmothnessAttributes, chosenSmothnessAttributes))
                    __target.wheelSetChooser.CurrentSetConfigurator.SmoothnessAttributes = chosenSmothnessAttributes;
            }
        }
        else
        {
            EditorGUILayout.LabelField("No wheel set chooser.", EditorStyles.helpBox);
        }

        GUILayout.Space(mediumEditorGUISpace);
        EditorGUILayout.LabelField("", EditorStyles.helpBox);
        GUILayout.Space(mediumEditorGUISpace);

        if (__target.frontLightsConfigurator || __target.interiorLightsConfigurator)
        {
            lightsConfiguratorFoldout = EditorGUILayout.Foldout(lightsConfiguratorFoldout, "Lights configuration");
            if (lightsConfiguratorFoldout)
            {
                if (__target.frontLightsConfigurator != null)
                {
                    if (GUILayout.Button("Switch front lights"))
                        __target.frontLightsConfigurator.SwitchLights();
                }

                if (__target.interiorLightsConfigurator != null)
                {
                    if (GUILayout.Button("Switch interior lights"))
                        __target.interiorLightsConfigurator.SwitchLights();
                }
            }
        }
        else
        {
            EditorGUILayout.LabelField("No lights.", EditorStyles.helpBox);
        }
    }

    private List<Color> ShowGUIForColors(List<Color> colors)
    {
        List<Color> chosenColors = new List<Color>();
        for (int i = 0; i < colors.Count; i++)
            chosenColors.Add(EditorGUILayout.ColorField("Chosen color", colors[i]));
        return chosenColors;
    }

    private List<float> ShowGUIForFloats(List<float> floats)
    {
        List<float> chosenFloats = new List<float>();
        for (int i = 0; i < floats.Count; i++)
            chosenFloats.Add(EditorGUILayout.Slider("Chosen value", floats[i], 0f, 1f));
        return chosenFloats;
    }
}

#endif