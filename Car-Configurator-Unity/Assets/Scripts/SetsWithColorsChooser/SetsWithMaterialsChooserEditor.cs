#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SetsWithMaterialsChooser))]
public class SetsWithMaterialsChooserEditor : AbstractSetsWithMaterialsChooserEditor<SetsWithMaterialsChooser, ConfiguratorWithMaterials>
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SetsWithMaterialsChooser __target = target as SetsWithMaterialsChooser;
    }
}

#endif