using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetsWithMaterialsChooser : AbstractSetsWithMaterialsChooser<ConfiguratorWithMaterials>
{
    public override List<Color> Colors 
    { 
        get
        {
            return CurrentSetConfigurator != null && !CurrentSetConfigurator.unchangeableColor
                ? CurrentSetConfigurator.Colors : new List<Color>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.Colors = value;
        }
    }

    public override List<float> MetallicAttributes
    {
        get
        {
            return CurrentSetConfigurator != null && !CurrentSetConfigurator.unchangebleMaterial
                ? CurrentSetConfigurator.MetallicAttributes : new List<float>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.MetallicAttributes = value;
        }
    }

    public override List<float> SmoothnessAttributes
    {
        get
        {
            return CurrentSetConfigurator != null && !CurrentSetConfigurator.unchangebleMaterial
                ? CurrentSetConfigurator.SmoothnessAttributes : new List<float>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.SmoothnessAttributes = value;
        }
    }
}
