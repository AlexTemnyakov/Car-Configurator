#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WheelConfigurator))]
public class WheelConfiguratorEditor : AbstractConfiguratorEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        WheelConfigurator __target = target as WheelConfigurator;

        if (GUILayout.Button("Initialize"))
            __target.Initialize();
    }
}

#endif