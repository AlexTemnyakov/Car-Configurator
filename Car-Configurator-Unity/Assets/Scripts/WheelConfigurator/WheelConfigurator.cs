using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelConfigurator : AbstractConfigurator
{
    public RimConfigurator rimConfigurator = null;

    public override void Initialize()
    {
        FindRimConfigurator();
    }

    private void FindRimConfigurator()
    {
        List<Transform> rimsTransforms = Utils.FindTransformsWithName(transform, ConfiguratorAdder.rimTransformName);

        if (rimsTransforms.Count != 1)
            throw new System.Exception("rimsTransforms.Count != 1");

        Transform rimTransform = rimsTransforms[0];

        rimConfigurator = rimTransform.gameObject.GetComponent<RimConfigurator>();

        if (rimConfigurator == null)
            throw new System.Exception("rimConfigurator == null");
    }    
}
