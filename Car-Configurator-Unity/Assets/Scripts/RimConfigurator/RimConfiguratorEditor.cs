#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RimConfigurator))]
public class RimConfiguratorEditor : AbstractConfiguratorWithMaterialEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        RimConfigurator __target = target as RimConfigurator;
    }
}

#endif