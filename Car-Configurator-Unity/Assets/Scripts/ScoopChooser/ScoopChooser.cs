using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoopChooser : SetsWithMaterialsChooser
{
    protected override void Awake()
    {
        base.Awake();

        CurrentSetNumber = (CurrentSetNumber - (CurrentSetNumber + 1));
    }
}
