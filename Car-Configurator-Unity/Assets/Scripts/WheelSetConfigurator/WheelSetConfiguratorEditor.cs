#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WheelSetConfigurator))]
public class WheelSetConfiguratorEditor : AbstractConfiguratorEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        WheelSetConfigurator __target = target as WheelSetConfigurator;

        if (GUILayout.Button("Initialize"))
            __target.Initialize();
    }
}

#endif