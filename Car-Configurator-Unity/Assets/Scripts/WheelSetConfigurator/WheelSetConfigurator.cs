using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class WheelSetConfigurator : AbstractConfigurator, IMaterialChanger
{
    public WheelConfigurator frontLeftWheelConfigurator = null;
    public WheelConfigurator frontRightWheelConfigurator = null;
    public WheelConfigurator rearLeftWheelConfigurator = null;
    public WheelConfigurator rearRightWheelConfigurator = null;

    public override void Initialize()
    {
        FindWheelConfigurators();
    }

    private void FindWheelConfigurators()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform childTransform = transform.GetChild(i);

            if (childTransform.name.Contains("front") && childTransform.name.Contains("left"))
                frontLeftWheelConfigurator = childTransform.gameObject.GetComponent<WheelConfigurator>();
            else if (childTransform.name.Contains("front") && childTransform.name.Contains("right"))
                frontRightWheelConfigurator = childTransform.gameObject.GetComponent<WheelConfigurator>();
            else if (childTransform.name.Contains("rear") && childTransform.name.Contains("left"))
                rearLeftWheelConfigurator = childTransform.gameObject.GetComponent<WheelConfigurator>();
            else if (childTransform.name.Contains("rear") && childTransform.name.Contains("right"))
                rearRightWheelConfigurator = childTransform.gameObject.GetComponent<WheelConfigurator>();
        }

        Assert.IsNotNull(frontLeftWheelConfigurator);
        Assert.IsNotNull(frontRightWheelConfigurator);
        Assert.IsNotNull(rearLeftWheelConfigurator);
        Assert.IsNotNull(rearRightWheelConfigurator);
    }

    public List<WheelConfigurator> WheelsConfigurators
    {
        get
        {
            return new List<WheelConfigurator>() 
            { 
                frontLeftWheelConfigurator, 
                frontRightWheelConfigurator, 
                rearRightWheelConfigurator, 
                rearLeftWheelConfigurator 
            };
        }
    }

    private List<Color> RimColors
    {
        get
        {
            return frontLeftWheelConfigurator.rimConfigurator.Colors;
        }

        set
        {
            foreach (var w in WheelsConfigurators)
                w.rimConfigurator.Colors = value;
        }
    }

    private List<float> RimMetallicAttributes
    {
        get
        {
            return frontLeftWheelConfigurator.rimConfigurator.MetallicAttributes;
        }

        set
        {
            foreach (var w in WheelsConfigurators)
                w.rimConfigurator.MetallicAttributes = value;
        }
    }

    private List<float> RimSmoothnessAttributes
    {
        get
        {
            return frontLeftWheelConfigurator.rimConfigurator.SmoothnessAttributes;
        }

        set
        {
            foreach (var w in WheelsConfigurators)
                w.rimConfigurator.SmoothnessAttributes = value;
        }
    }

    public bool UnchangeableRimColor
    {
        get
        {
            return frontLeftWheelConfigurator.rimConfigurator.unchangeableColor;
        }

        set
        {
            foreach (var w in WheelsConfigurators)
                w.rimConfigurator.unchangeableColor = value;
        }
    }

    public bool UnchangeableRimMaterial
    {
        get
        {
            return frontLeftWheelConfigurator.rimConfigurator.unchangebleMaterial;
        }

        set
        {
            foreach (var w in WheelsConfigurators)
                w.rimConfigurator.unchangebleMaterial = value;
        }
    }

    public List<Color> Colors 
    { 
        get
        {
            return RimColors;
        }

        set
        {
            RimColors = value;
        }
    }

    public List<float> MetallicAttributes
    {
        get
        {
            return RimMetallicAttributes;
        }

        set
        {
            RimMetallicAttributes = value;
        }
    }

    public List<float> SmoothnessAttributes
    {
        get
        {
            return RimSmoothnessAttributes;
        }

        set
        {
            RimSmoothnessAttributes = value;
        }
    }
}
