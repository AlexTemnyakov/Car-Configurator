using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractCarLightsConfigurator : AbstractConfigurator
{
    public List<Light> lights = new List<Light>();

    public float intensityWhenOn = 1f;

    public override void Initialize()
    {
        lights.Clear();
        lights.AddRange(transform.GetComponentsInChildren<Light>());
        if (lights.Count > 0)
        {
            intensityWhenOn = lights[0].intensity;
            if (lights[0].intensity > 0.0001f)
                SwitchLights();
        }
    }

    public virtual void SwitchLights()
    {
        foreach (var l in lights)
            l.intensity = intensityWhenOn - l.intensity;
    }
}
