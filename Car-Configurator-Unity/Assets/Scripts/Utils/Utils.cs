using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Auxiliary functions.
/// </summary>
public class Utils : MonoBehaviour
{
    public static List<Transform> FindTransformsWithName(string requiredName, bool startString = false)
    {
        GameObject[] gameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();

        List<Transform> ret = new List<Transform>();

        foreach (var o in gameObjects)
            ret.AddRange(FindTransformsWithName(o.transform, requiredName, startString));

        return ret;
    }

    public static List<Transform> FindTransformsWithName(Transform root, string requiredName, bool startString = false)
    {
        List<Transform> ret = new List<Transform>();

        _FindTransformsWithName(root, requiredName, startString, ref ret);

        return ret;
    }

    private static void _FindTransformsWithName(Transform currentTransform, string requiredName, bool startString, ref List<Transform> listOfTransformsWithRequiredName)
    {
        if (startString)
        {
            if (currentTransform.name.StartsWith(requiredName))
                listOfTransformsWithRequiredName.Add(currentTransform);
        }
        else
        {
            if (currentTransform.name == requiredName)
                listOfTransformsWithRequiredName.Add(currentTransform);
        }

        for (int i = 0; i < currentTransform.childCount; i++)
            _FindTransformsWithName(currentTransform.GetChild(i), requiredName, startString, ref listOfTransformsWithRequiredName);
    }

    public static List<Transform> GetAllChildTransforms(Transform root)
    {
        List<Transform> ret = new List<Transform>();

        _GetAllChildTransforms(root, ref ret);

        return ret;
    }

    private static void _GetAllChildTransforms(Transform currentTransform, ref List<Transform> transforms)
    {
        for (int i = 0; i < currentTransform.childCount; i++)
        {
            transforms.Add(currentTransform.GetChild(i));
            _GetAllChildTransforms(currentTransform.GetChild(i), ref transforms);
        }
    }

    public static List<T> FindObjectsWithComponent<T>() where T : Component
    {
        List<GameObject> gameObjects;

        GameObject[] _gameObjects = GameObject.FindObjectsOfType<GameObject>();

        gameObjects = new List<GameObject>(_gameObjects);

        List<T> ret = new List<T>();

        foreach (var o in gameObjects)
            if (o.GetComponent<T>() != null)
                ret.Add(o.GetComponent<T>());

        return ret;
    }

    private static List<GameObject> guiControlElementsGameObjects = null;

    public static List<GameObject> GUIControlElements
    {
        get
        {
            if (guiControlElementsGameObjects == null)
                guiControlElementsGameObjects = new List<GameObject>(GameObject.FindGameObjectsWithTag("GUI Control Element"));
            return guiControlElementsGameObjects;
        }
    }
}
