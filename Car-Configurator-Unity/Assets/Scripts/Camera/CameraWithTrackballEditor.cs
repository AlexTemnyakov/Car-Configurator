#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CameraWithTrackball))]
public class CameraWithTrackballEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CameraWithTrackball __target = target as CameraWithTrackball;

        if (GUILayout.Button("To car wheel"))
            __target.ToCarFLWheel();

        if (GUILayout.Button("To car body"))
            __target.ToCarBody();
    }
}

#endif