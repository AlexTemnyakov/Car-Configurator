using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A manager for the cars.
/// It enables the required car and disables the others.
/// </summary>
public class CarChooser : MonoBehaviour
{
    // The script finds these configurators automatically. They must be under the transformation of this object.
    public List<CarConfigurator> carConfigurators = new List<CarConfigurator>();
    public int currentCarNumber = 0;

    void Awake()
    {
        Initialize();
        ShowCurrentCar();
    }

    void Update()
    {
        
    }

    public void Initialize()
    {
        carConfigurators.AddRange(transform.GetComponentsInChildren<CarConfigurator>());
    }

    public void NextCar()
    {
        if (currentCarNumber + 1 < carConfigurators.Count)
            currentCarNumber++;
        ShowCurrentCar();
    }

    public void PreviousCar()
    {
        if (currentCarNumber - 1 >= 0)
            currentCarNumber--;
        ShowCurrentCar();
    }

    private void ShowCurrentCar()
    {
        foreach (var c in carConfigurators)
            c.gameObject.SetActive(false);
        carConfigurators[currentCarNumber].gameObject.SetActive(true);
    }

    public CarConfigurator CurrentCarConfigurator
    {
        get
        {
            return carConfigurators[currentCarNumber];
        }
    }
}
