using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// This class allows to add all configurators automatically.
/// </summary>
public class ConfiguratorAdder : MonoBehaviour
{
    public readonly static string rimTransformName = "rim";
    public readonly static string wheelTransformNameStartString = "wheel_";
    public readonly static string wheelSetTransformNameStartString = "wheels_set_";
    public readonly static string wheelSetsTransformName = "wheels_sets";
    public readonly static string bodyTransformName = "body";
    public readonly static string spoilerTransformNameStartString = "spoiler_";
    public readonly static string spoilersTransformName = "spoilers";
    public readonly static string scoopTransformNameStartString = "scoop_";
    public readonly static string scoopsTransformName = "scoops";
    public readonly static string carTransformNameStartString = "car";
    public readonly static string frontLightsRootTransformName = "front_lights";
    public readonly static string interiorLightsRootTransformName = "interior_lights";

    public static void AddAllConfigurators()
    {
        AddFrontLightsConfigurators();
        AddInteriorLightsConfigurators();
        AddRimConfigurators();
        AddWheelConfigurators();
        AddWheelSetConfigurators();
        AddWheelSetChooser();
        AddBodyConfigurators();
        AddSpoilerConfigurators();
        AddSpoilerChoosers();
        AddScoopsConfigurators();
        AddScoopChoosers();
        AddCarConfigurators();
    }

    public static void InitializeAllConfigurators()
    {
        InitializeFrontLightsConfigurators();
        InitializeInteriorLightsConfigurators();
        InitializeRimConfigurators();
        InitializeWheelConfigurators();
        InitializeWheelSetConfigurators();
        InitializeWheelSetChooser();
        InitializeBodyConfigurators();
        InitializeSpoilerConfigurators();
        InitializeSpoilerChoosers();
        InitializeScoopsConfigurators();
        InitializeScoopChoosers();
        InitializeCarConfigurators();
    }

    public static void DestroyAllConfigurators()
    {
        DestroyFrontLightsConfigurators();
        DestroyInteriorLightsConfigurators();
        DestroyRimConfigurators();
        DestroyWheelConfigurators();
        DestroyWheelSetConfigurators();
        DestroyWheelSetChooser();
        DestroyBodyConfigurators();
        DestroySpoilerConfigurators();
        DestroySpoilerChoosers();
        DestroyScoopsConfigurators();
        DestroyScoopChoosers();
        DestroyCarConfigurators();
    }

    public static void AddFrontLightsConfigurators()
    {
        AddConfigurators<CarFrontLightsConfigurator>(frontLightsRootTransformName, false);
    }

    public static void InitializeFrontLightsConfigurators()
    {
        InitializeConfigurators<CarFrontLightsConfigurator>();
    }

    public static void DestroyFrontLightsConfigurators()
    {
        DestroyConfigurators<CarFrontLightsConfigurator>();
    }

    public static void AddInteriorLightsConfigurators()
    {
        AddConfigurators<CarInteriorLightsConfigurator>(interiorLightsRootTransformName, false);
    }

    public static void InitializeInteriorLightsConfigurators()
    {
        InitializeConfigurators<CarInteriorLightsConfigurator>();
    }

    public static void DestroyInteriorLightsConfigurators()
    {
        DestroyConfigurators<CarInteriorLightsConfigurator>();
    }

    public static void AddRimConfigurators()
    {
        AddConfigurators<RimConfigurator>(rimTransformName, false);
    }

    public static void InitializeRimConfigurators()
    {
        InitializeConfigurators<RimConfigurator>();
    }

    public static void DestroyRimConfigurators()
    {
        DestroyConfigurators<RimConfigurator>();
    }

    public static void AddWheelConfigurators()
    {
        AddConfigurators<WheelConfigurator>(wheelTransformNameStartString, true);
    }

    public static void InitializeWheelConfigurators()
    {
        InitializeConfigurators<WheelConfigurator>();
    }

    public static void DestroyWheelConfigurators()
    {
        DestroyConfigurators<WheelConfigurator>();
    }

    public static void AddWheelSetConfigurators()
    {
        AddConfigurators<WheelSetConfigurator>(wheelSetTransformNameStartString, true);
    }

    public static void InitializeWheelSetConfigurators()
    {
        InitializeConfigurators<WheelSetConfigurator>();
    }

    public static void DestroyWheelSetConfigurators()
    {
        DestroyConfigurators<WheelSetConfigurator>();
    }

    public static void AddWheelSetChooser()
    {
        AddConfigurators<WheelSetChooser>(wheelSetsTransformName, false);
    }

    public static void InitializeWheelSetChooser()
    {
        InitializeConfigurators<WheelSetChooser>();
    }

    public static void DestroyWheelSetChooser()
    {
        DestroyConfigurators<WheelSetChooser>();
    }

    public static void AddBodyConfigurators()
    {
        AddConfigurators<BodyConfigurator>(bodyTransformName, false);
    }

    public static void InitializeBodyConfigurators()
    {
        InitializeConfigurators<BodyConfigurator>();
    }

    public static void DestroyBodyConfigurators()
    {
        DestroyConfigurators<BodyConfigurator>();
    }

    public static void AddSpoilerConfigurators()
    {
        AddConfigurators<ConfiguratorWithMaterials>(spoilerTransformNameStartString, true);
    }

    public static void InitializeSpoilerConfigurators()
    {
        InitializeConfigurators<ConfiguratorWithMaterials>();
    }

    public static void DestroySpoilerConfigurators()
    {
        DestroyConfigurators<ConfiguratorWithMaterials>();
    }

    public static void AddScoopsConfigurators()
    {
        AddConfigurators<ConfiguratorWithMaterials>(scoopTransformNameStartString, true);
    }

    public static void InitializeScoopsConfigurators()
    {
        InitializeConfigurators<ConfiguratorWithMaterials>();
    }

    public static void DestroyScoopsConfigurators()
    {
        DestroyConfigurators<ConfiguratorWithMaterials>();
    }

    public static void AddSpoilerChoosers()
    {
        AddConfigurators<SpoilerChooser>(spoilersTransformName, false);
    }

    public static void InitializeSpoilerChoosers()
    {
        InitializeConfigurators<SpoilerChooser>();
    }

    public static void DestroySpoilerChoosers()
    {
        DestroyConfigurators<SpoilerChooser>();
    }

    public static void AddScoopChoosers()
    {
        AddConfigurators<ScoopChooser>(scoopsTransformName, false);
    }

    public static void InitializeScoopChoosers()
    {
        InitializeConfigurators<ScoopChooser>();
    }

    public static void DestroyScoopChoosers()
    {
        DestroyConfigurators<ScoopChooser>();
    }

    public static void AddCarConfigurators()
    {
        AddConfigurators<CarConfigurator>(carTransformNameStartString, true);
    }

    public static void InitializeCarConfigurators()
    {
        InitializeConfigurators<CarConfigurator>();
    }

    public static void DestroyCarConfigurators()
    {
        DestroyConfigurators<CarConfigurator>();
    }

    private static void AddConfigurators<T>(string transformName, bool startString = false) where T : Component
    {
        List<Transform> transforms = Utils.FindTransformsWithName(transformName, startString);

        AddConfigurators<T>(transforms);
    }

    private static void InitializeConfigurators<T>() where T : AbstractConfigurator
    {
        List<T> objects = Utils.FindObjectsWithComponent<T>();

        foreach (var o in objects)
        {
            o.Initialize();
        }
    }

    private static void DestroyConfigurators<T>() where T : Component
    {
        foreach (var o in Utils.FindObjectsWithComponent<T>())
            DestroyImmediate(o.GetComponent<T>());
    }

    private static void AddConfigurators<T>(List<Transform> transforms) where T : Component
    {
        foreach (var t in transforms)
        {
            T configurator = t.gameObject.GetComponent<T>();

            if (configurator != null)
                DestroyImmediate(configurator);

            configurator = t.gameObject.AddComponent<T>();
        }
    }
}
