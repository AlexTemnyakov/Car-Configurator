#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ConfiguratorAdder))]
public class ConfiguratorAdderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Add all configurators"))
            ConfiguratorAdder.AddAllConfigurators();

        if (GUILayout.Button("Initialize all configurators"))
            ConfiguratorAdder.InitializeAllConfigurators();

        if (GUILayout.Button("Destroy all configurators"))
            ConfiguratorAdder.DestroyAllConfigurators();
    }
}

#endif