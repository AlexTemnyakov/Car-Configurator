using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// This class is a nonabstract variant of the abstract class for car element configurators
/// that allows to change a material's parameters.
/// </summary>
public class ConfiguratorWithMaterials : AbstractConfiguratorWithMaterial
{

}
