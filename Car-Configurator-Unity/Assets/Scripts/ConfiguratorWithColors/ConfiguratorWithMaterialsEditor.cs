#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ConfiguratorWithMaterials))]
public class ConfiguratorWithMaterialsEditor : AbstractConfiguratorWithMaterialEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ConfiguratorWithMaterials __target = target as ConfiguratorWithMaterials;
    }
}

#endif