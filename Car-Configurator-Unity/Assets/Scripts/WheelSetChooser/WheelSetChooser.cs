using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class WheelSetChooser : AbstractSetsWithMaterialsChooser<WheelSetConfigurator>
{
    public override List<Color> Colors 
    { 
        get
        {
            return CurrentSetConfigurator.Colors;
        }

        set
        {
            CurrentSetConfigurator.Colors = value;
        }
    }

    public override List<float> MetallicAttributes
    {
        get
        {
            return CurrentSetConfigurator != null ? CurrentSetConfigurator.MetallicAttributes : new List<float>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.MetallicAttributes = value;
        }
    }

    public override List<float> SmoothnessAttributes
    {
        get
        {
            return CurrentSetConfigurator != null ? CurrentSetConfigurator.SmoothnessAttributes : new List<float>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.SmoothnessAttributes = value;
        }
    }
}
