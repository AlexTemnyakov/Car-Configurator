#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WheelSetChooser))]
public class WheelSetChooserEditor : AbstractSetsWithMaterialsChooserEditor<WheelSetChooser, WheelSetConfigurator>
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        WheelSetChooser __target = target as WheelSetChooser;
    }
}

#endif