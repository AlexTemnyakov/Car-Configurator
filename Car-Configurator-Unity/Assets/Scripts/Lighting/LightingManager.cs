using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class to set daytime or the nighttime.
/// </summary>
public class LightingManager : MonoBehaviour
{
    public Light directionalLight;

    private EnvironmentColors dayColors;
    private EnvironmentColors nightColors = new EnvironmentColors ()
    { 
        skyColor = Color.black, 
        equatorColor = Color.black, 
        groundColor = Color.black 
    };
    private bool isDay = true;
    private float lightIntensity;
    private Vector3 lightDirectionAtBeginning;

    void Start()
    {
        RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;

        dayColors = new EnvironmentColors()
        {
            skyColor = RenderSettings.ambientSkyColor,
            equatorColor = RenderSettings.ambientEquatorColor,
            groundColor = RenderSettings.ambientGroundColor
        };

        lightIntensity = directionalLight.intensity;
        lightDirectionAtBeginning = directionalLight.transform.forward;
    }

    void Update()
    {

    }

    public void SwitchDayNight()
    {
        isDay = !isDay;

		float intensityMultiplier = 1;

		if (isDay)
        {
			intensityMultiplier = 1f;
            RenderSettings.ambientSkyColor = dayColors.skyColor;
            RenderSettings.ambientEquatorColor = dayColors.equatorColor;
            RenderSettings.ambientGroundColor = dayColors.groundColor;
            directionalLight.transform.forward = lightDirectionAtBeginning;
        }
		else
		{
			intensityMultiplier = 0f;
            RenderSettings.ambientSkyColor = nightColors.skyColor;
            RenderSettings.ambientEquatorColor = nightColors.equatorColor;
            RenderSettings.ambientGroundColor = nightColors.groundColor;
            directionalLight.transform.forward = -lightDirectionAtBeginning;
        }

		directionalLight.intensity = lightIntensity * intensityMultiplier;
	}
}

[System.Serializable]
public class EnvironmentColors
{
    public Color skyColor;
    public Color equatorColor;
    public Color groundColor;
}