using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// A configurator that allows adjusting a material's color, metallic and smoothness attributes.
/// 
/// The body configurator inherits this class. It's possible to change the body's material properties.
/// </summary>
public class AbstractConfiguratorWithMaterial : AbstractConfigurator, IMaterialChanger
{
    [Tooltip("Check if the color mustn't be changed.")]
    public bool unchangeableColor = false;
    [Tooltip("Check if the metallic and smoothness attributes mustn't be changed.")]
    public bool unchangebleMaterial = false;

    [SerializeField]
    protected MeshRenderer meshRenderer = null;

    public override void Initialize()
    {
        meshRenderer = GetComponent<MeshRenderer>();

        Assert.IsNotNull(meshRenderer);
    }

    protected List<Material> UniqueMaterials
    {
        get
        {
            List<Material> materials = new List<Material>();
            HashSet<string> uniqueMaterialNames = new HashSet<string>();

            bool useSharedMaterials = !Application.isPlaying;

            Material[] materialsArray = useSharedMaterials ? meshRenderer.sharedMaterials : meshRenderer.materials;

            foreach (var mat in materialsArray)
            {
                if (!uniqueMaterialNames.Contains(mat.name))
                {
                    uniqueMaterialNames.Add(mat.name);
                    materials.Add(mat);
                }
            }

            return materials;
        }
    }

    /// <summary>
    /// Get/set the colors for the materials.
    /// </summary>
    public List<Color> Colors
    {
        get
        {
            List<Color> colors = new List<Color>();

            if (!unchangeableColor)
                foreach (var mat in UniqueMaterials)
                    colors.Add(mat.color);

            return colors;
        }

        set
        {
            if (unchangeableColor)
                return;

            Assert.AreEqual(UniqueMaterials.Count, value.Count);

            Dictionary<string, Color> materialsAndColors = new Dictionary<string, Color>();

            for (int i = 0; i < UniqueMaterials.Count; i++)
            {
                var mat = UniqueMaterials[i];
                var color = value[i];

                materialsAndColors[mat.name] = color;
            }

            foreach (var mat in Application.isPlaying ? meshRenderer.materials : meshRenderer.sharedMaterials)
                mat.color = materialsAndColors[mat.name];
        }
    }

    /// <summary>
    /// Get/set the metallic attributes for the materials.
    /// </summary>
    public List<float> MetallicAttributes
    {
        get
        {
            List<float> metallicAttributes = new List<float>();

            if (!unchangebleMaterial)
                foreach (var mat in UniqueMaterials)
                    metallicAttributes.Add(mat.GetFloat("_Metallic"));

            return metallicAttributes;
        }

        set
        {
            if (unchangebleMaterial)
                return;

            Assert.AreEqual(UniqueMaterials.Count, value.Count);

            Dictionary<string, float> materialsAndFloats = new Dictionary<string, float>();

            for (int i = 0; i < UniqueMaterials.Count; i++)
            {
                var mat = UniqueMaterials[i];
                var f = value[i];

                materialsAndFloats[mat.name] = f;
            }

            foreach (var mat in Application.isPlaying ? meshRenderer.materials : meshRenderer.sharedMaterials)
                mat.SetFloat("_Metallic", materialsAndFloats[mat.name]);
        }
    }

    /// <summary>
    /// Get/set the smoothness attributes for the materials.
    /// </summary>
    public List<float> SmoothnessAttributes
    {
        get
        {
            List<float> metallicAttributes = new List<float>();

            if (!unchangebleMaterial)
                foreach (var mat in UniqueMaterials)
                    metallicAttributes.Add(mat.GetFloat("_Glossiness"));

            return metallicAttributes;
        }

        set
        {
            if (unchangebleMaterial)
                return;

            Assert.AreEqual(UniqueMaterials.Count, value.Count);

            Dictionary<string, float> materialsAndFloats = new Dictionary<string, float>();

            for (int i = 0; i < UniqueMaterials.Count; i++)
            {
                var mat = UniqueMaterials[i];
                var f = value[i];

                materialsAndFloats[mat.name] = f;
            }

            foreach (var mat in Application.isPlaying ? meshRenderer.materials : meshRenderer.sharedMaterials)
                mat.SetFloat("_Glossiness", materialsAndFloats[mat.name]);
        }
    }
}
