using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This interface requires to implement C# properties to change a material's parameters.
/// </summary>
public interface IMaterialChanger
{
    public List<Color> Colors
    {
        get;
        set;
    }

    public List<float> MetallicAttributes
    {
        get;
        set;
    }

    public List<float> SmoothnessAttributes
    {
        get;
        set;
    }
}
