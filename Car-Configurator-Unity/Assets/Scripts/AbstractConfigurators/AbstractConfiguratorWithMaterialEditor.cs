#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AbstractConfiguratorWithMaterial))]
public class AbstractConfiguratorWithMaterialEditor : AbstractConfiguratorEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        AbstractConfiguratorWithMaterial __target = target as AbstractConfiguratorWithMaterial;

        List<Color> previousColors = __target.Colors;
        List<Color> chosenColors = new List<Color>();
        for (int i = 0; i < __target.Colors.Count; i++)
            chosenColors.Add(EditorGUILayout.ColorField("Chosen color", __target.Colors[i]));

        if (!Enumerable.SequenceEqual(previousColors, chosenColors))
            __target.Colors = chosenColors;
    }
}

#endif
