using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractConfigurator : MonoBehaviour
{
    protected virtual void Awake()
    {
        Initialize();
    }

    public abstract void Initialize();
}
