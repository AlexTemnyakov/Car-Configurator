using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// A manager for the chooser of car elements. These elements are supposed to be with materials.
/// This class may change the properties of the car elements below it.
/// 
/// For example, the wheels inherit this class. It's possible to change the wheel objects and their materials.
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class AbstractSetsWithMaterialsChooser<T> : AbstractConfigurator where T : AbstractConfigurator, IMaterialChanger
{
    public List<T> setsConfigurators = new List<T>();

    protected int currentSetNumber = 0;

    protected virtual void Start()
    {
        ChangeCurrentSet();
    }

    protected virtual void Update()
    {
        
    }

    public override void Initialize()
    {
        FindSetsConfigurators();
    }

    protected virtual void FindSetsConfigurators()
    {
        setsConfigurators.Clear();

        foreach (var o in transform.GetComponentsInChildren<T>())
            setsConfigurators.Add(o.GetComponent<T>());
    }

    protected virtual void ChangeCurrentSet()
    {
        foreach (var o in setsConfigurators)
            o.gameObject.SetActive(false);

        if (CurrentSetNumber >= 0)
            setsConfigurators[CurrentSetNumber].gameObject.SetActive(true);
    }

    public virtual T CurrentSetConfigurator
    {
        get
        {
            return CurrentSetNumber >= 0 ? setsConfigurators[CurrentSetNumber] : null;
        }
    }

    public virtual int CurrentSetNumber
    {
        get
        {
            return currentSetNumber;
        }

        set
        {
            int prev = currentSetNumber;

            currentSetNumber = value;

            if (prev != currentSetNumber)
                ChangeCurrentSet();
        }
    }

    public virtual List<Color> Colors
    {
        get
        {
            return CurrentSetConfigurator != null ? CurrentSetConfigurator.Colors : new List<Color>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.Colors = value;
        }
    }

    public virtual List<float> MetallicAttributes
    {
        get
        {
            return CurrentSetConfigurator != null ? CurrentSetConfigurator.MetallicAttributes : new List<float>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.MetallicAttributes = value;
        }
    }

    public virtual List<float> SmoothnessAttributes
    {
        get
        {
            return CurrentSetConfigurator != null ? CurrentSetConfigurator.SmoothnessAttributes : new List<float>();
        }

        set
        {
            if (CurrentSetConfigurator != null)
                CurrentSetConfigurator.SmoothnessAttributes = value;
        }
    }
}
