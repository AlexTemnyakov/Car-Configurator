﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public abstract class AbstractSetsWithMaterialsChooserEditor<T1, T2> : AbstractConfiguratorEditor where T1 : AbstractSetsWithMaterialsChooser<T2> where T2 : AbstractConfigurator, IMaterialChanger
{    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        T1 __target = target as T1;

        __target.CurrentSetNumber = EditorGUILayout.IntSlider("Current set", __target.CurrentSetNumber, 0, __target.setsConfigurators.Count - 1);
    }
}

#endif
